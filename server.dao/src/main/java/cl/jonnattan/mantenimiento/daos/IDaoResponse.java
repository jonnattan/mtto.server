package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Response;

public interface IDaoResponse extends CrudRepository<Response, Long>
{
   
}
