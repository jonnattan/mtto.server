package cl.jonnattan.mantenimiento.daos;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Persona;

public interface IDaoPerson extends CrudRepository<Persona, Long>
{
  Optional<Persona> findAllByUserAndPassword( String user, String password);
  Optional<Persona> findAllByUser( String user);
}
