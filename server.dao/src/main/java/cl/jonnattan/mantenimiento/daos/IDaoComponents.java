package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Component;

public interface IDaoComponents extends CrudRepository<Component, Long>
{
  // espectacular
}
