package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Equipment;

public interface IDaoEquipment extends CrudRepository<Equipment, Long>
{
  // espectacular
}
