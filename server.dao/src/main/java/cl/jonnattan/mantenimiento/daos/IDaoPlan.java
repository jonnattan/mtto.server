package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.PlanMtto;

public interface IDaoPlan extends CrudRepository<PlanMtto, Long>
{
  // espectacular
}
