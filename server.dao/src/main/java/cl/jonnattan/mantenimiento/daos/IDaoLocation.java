package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Location;

public interface IDaoLocation extends CrudRepository<Location, Long>
{
  // espectacular
}
