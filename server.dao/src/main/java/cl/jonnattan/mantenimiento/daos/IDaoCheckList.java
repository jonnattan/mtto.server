package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;
import cl.jonnattan.mantenimiento.CheckList;

public interface IDaoCheckList extends CrudRepository<CheckList, Long>
{
  // espectacular
}
