package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.ReportMtto;

public interface IDaoReportMtto extends CrudRepository<ReportMtto, Long>
{
  
}
