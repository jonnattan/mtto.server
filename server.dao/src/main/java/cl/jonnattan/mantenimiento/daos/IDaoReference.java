package cl.jonnattan.mantenimiento.daos;

import org.springframework.data.repository.CrudRepository;

import cl.jonnattan.mantenimiento.Reference;

public interface IDaoReference extends CrudRepository<Reference, Long>
{
  // espectacular
}
