package cl.jonnattan.mantenimiento;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table(name = "personas")
@Data
public class Persona
{
  @Id
  @Column(name = "id_persona", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nombre")
  private String name;

  @Column(name = "cargo")
  private String rol;

  @Column(name = "usuario")
  private String user;

  @Column(name = "pass")
  private String password;
  
  @Column(name = "correo")
  private String mail;

  @Column(name = "edad")
  private Integer age;

  @Column(name = "fecha_creacion")
  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
  private Date creation;

  public Persona()
  {
    // TODO Auto-generated constructor stub
  }

  @PrePersist
  @PreUpdate
  public void prePersist()
  {
    creation = new Date();
  }

}
