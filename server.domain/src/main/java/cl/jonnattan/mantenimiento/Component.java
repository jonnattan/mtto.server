package cl.jonnattan.mantenimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "Componentes")
public class Component implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_componente", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nombre")
  private String name;

  @Column(name = "numero_parte")
  private String partNumber;

  @Column(name = "numero_serie")
  private String serialNumber;

  @Column(name = "descripcion")
  private String description;

  @Column(name = "cantidad")
  private Integer quantity;
  
  @JoinColumn(name = "id_equipo", nullable = false)
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
  private Equipment equipo;

  public Component()
  {

  }

}
