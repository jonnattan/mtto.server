package cl.jonnattan.mantenimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "checklist")
public class CheckList implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_checklist", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Column(name = "titulo")
  private String title;
  
  @Column(name = "descripcion")
  private String description;
  
  @JoinColumn(name = "id_plan_mtto", nullable = false)
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
  private PlanMtto plan;
  
  public CheckList()
  {
    // TODO Auto-generated constructor stub
  }

}
