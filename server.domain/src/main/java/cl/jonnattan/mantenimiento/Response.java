package cl.jonnattan.mantenimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "respuesta_mtto")
public class Response implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_respuesta", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_checklist", referencedColumnName = "id_checklist")
  private CheckList check;
  
  @Column(name = "respuesta_check")
  private Boolean checked;
  
  @Column(name = "date")
  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
  private Date date;
  
  
  public Response()
  {
    super();
  }
  
  @PrePersist
  public void prePersist()
  {
    date = new Date();
  }

}
