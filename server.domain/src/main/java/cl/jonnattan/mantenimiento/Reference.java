package cl.jonnattan.mantenimiento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "planos_referencia")
public class Reference implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_plano_ref", nullable = false, unique = true)
  @GeneratedValue( strategy=GenerationType.IDENTITY )
  private Long id;
  
  @Column(name = "nombre_plano")
  private String name;
  
  @Column(name = "ruta_plano")
  private String path;
  
  @Column(name = "extension_archivo")
  private String extend;

  public Reference()
  {
    
  }

}
