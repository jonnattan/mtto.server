package cl.jonnattan.mantenimiento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "plan_mtto")
public class PlanMtto implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_plan_mtto", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nombre")
  private String name;
  
  @Column(name = "version")
  private String version;
  
  @Column(name = "periodo_dias")
  private Integer periodo;
  
  @Column(name = "ultima_rev")
  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
  private Date lastRev;
  
  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_plan_mtto")
  private List<CheckList> checklists;
  
  public PlanMtto()
  {
    checklists = new ArrayList<>();
  }
  
  @PrePersist
  @PreUpdate
  public void prePersist()
  {
    lastRev = new Date();
  }

}
