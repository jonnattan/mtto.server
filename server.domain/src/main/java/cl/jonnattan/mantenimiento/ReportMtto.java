package cl.jonnattan.mantenimiento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "reporte_mtto")
public class ReportMtto implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_reporte_mtto", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Column(name = "pasada")
  private Boolean ok;
  
  @JoinColumn(name = "id_equipo", nullable = false)
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
  private Equipment equipo;
  
  @Column(name = "con_falla")
  private Boolean failure;
  
  @Column(name = "comentario")
  private String comment;
  
  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
  private Persona person;
  
  @Column(name = "razon_falla")
  private String reasonFailure;
  
  @Column(name = "requiere_externo")
  private Boolean externAgent; 
  
  @Column(name = "fecha_mtto")
  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
  private Date date;
  
  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_reporte_mtto")
  @OrderBy("date DESC")
  private List<Response> reports;
  
  public ReportMtto()
  {
    reports = new ArrayList<>();
  }

  @PrePersist
  @PreUpdate
  public void prePersist()
  {
    date = new Date();
  }

}
