package cl.jonnattan.mantenimiento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table(name = "equipos")
/*
 * @NamedQueries({
 * 
 * @NamedQuery(name = "Neighbour.findByImei", query =
 * "SELECT e FROM Equipment e WHERE e.numero_parte = ?1 "),
 * 
 * @NamedQuery(name = "Neighbour.findAll", query = "SELECT c FROM Equipment c")
 * })
 */
@Data
public class Equipment implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id_equipo", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long              id;

  @Column(name = "nombre")
  private String name;

  @Column(name = "numero_parte")
  private String partnumber;

  @Column(name = "numero_serie")
  private String serialNumber;

  private String tag;

  @Column(name = "fecha_instalacion")
  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
  private Date install;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_ubicacion", referencedColumnName = "id_ubicacion")
  private Location location;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_plano_ref", referencedColumnName = "id_plano_ref")
  private Reference reference;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_plan_mtto", referencedColumnName = "id_plan_mtto")
  private PlanMtto plan;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_equipo")
  private List<Component> components;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_equipo")
  @OrderBy("fecha_mtto DESC")
  private List<ReportMtto> reports;

  public Equipment()
  {
    components = new ArrayList<>();
    reports = new ArrayList<>();
  }

  @PrePersist
  public void prePersist()
  {
    install = new Date();
  }
}
