package cl.jonnattan.mantenimiento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ubicaciones")
public class Location implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id_ubicacion", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "descripcion_lugar")
  private String description;

  @Column(name = "zona_ubicacion")
  private String zone;

  public Location()
  {

  }
}
