package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * Clase sin descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since Programa de Mantenimiento
 * @version 1.0 del 21-11-2019
 * 
 */
@EqualsAndHashCode(callSuper=true)
public @Data class jPerson extends AbsKeylable
{
  @NonNull
  private String nombre;
  
  @NonNull
  private String correo;
  
  @NonNull
  private String cargo;
  
  @NonNull
  private Integer edad;
  
  @NonNull
  private String user;
  
  @NonNull
  private String pass;
  
  public jPerson()
  {
    super();
  }

}
