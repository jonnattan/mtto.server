package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=true)
public @Data class jLocation extends AbsKeylable
{  
  private String lugar;
  private String zona;

  public jLocation()
  {
    super();
  }

}

