package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jChecklist extends AbsKeylable
{
  private String titulo;
  private String descripcion;
  private Long   idPlan;

  public jChecklist()
  {
    super();
  }
}
