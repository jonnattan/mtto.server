package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * Clase sin descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since Programa de Mantenimiento
 * @version 1.0 del 02-01-2020
 * 
 */
@EqualsAndHashCode(callSuper = true)
public @Data class jCheckResp extends AbsKeylable
{
  @NonNull
  private Boolean yes;

  public jCheckResp()
  {
    super();
  }
}
