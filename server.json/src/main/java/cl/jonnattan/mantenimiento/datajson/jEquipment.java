package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=true)
public @Data class jEquipment extends AbsKeylable
{  
  private String name;
  private String partNumber;
  private String serialNumber;
  private String lastDetail;
  private String namePlan;

  public jEquipment()
  {
    super();
  }

}

