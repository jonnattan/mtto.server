package cl.jonnattan.mantenimiento.datajson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jPlan extends AbsKeylable
{
  private String           name;
  private String           version;
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private Date             revision;
  private List<jChecklist> items;

  public jPlan()
  {
    super();
    items = new ArrayList<>();
  }
  
  
}
