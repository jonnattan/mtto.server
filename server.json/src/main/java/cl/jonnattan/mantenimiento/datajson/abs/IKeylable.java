package cl.jonnattan.mantenimiento.datajson.abs;

/**
 * Interfaz que se utiliza para objetos que son tuplas de una BD
 * 
 * @author Jonnattan Griffiths
 * @since Programa de Mantenimiento
 * @version 1.0 del 22-12-2019
 * 
 */
public interface IKeylable
{
  public Long getId();
}
