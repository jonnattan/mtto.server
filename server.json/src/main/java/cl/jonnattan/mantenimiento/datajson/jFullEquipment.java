package cl.jonnattan.mantenimiento.datajson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jFullEquipment extends AbsKeylable
{
  private String           name;
  private String           partNumber;
  private String           serialNumber;
  private String           tag;
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private Date             installDate;
  private jLocation        location;
  private jReference       reference;
  private jPlan            plan;
  private List<jComponent> components;
  private List<jReport>    maintenances;

  public jFullEquipment()
  {
    super();
    maintenances = new ArrayList<>();
    components = new ArrayList<>();
  }

}
