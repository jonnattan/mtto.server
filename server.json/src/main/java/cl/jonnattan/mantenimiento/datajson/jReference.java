package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=true)
public @Data class jReference extends AbsKeylable
{  
  private String name;
  private String path;
  private String extend;
  
  public jReference()
  {
    super();
  }

}

