package cl.jonnattan.mantenimiento.datajson;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jComponent extends AbsKeylable
{
  private String  name;
  private String  partnumber;
  private String  serialNumber;
  private String  descripcion;
  private Integer cantidad;
  private Long    idEq;

  public jComponent()
  {
    super();
  }

}
