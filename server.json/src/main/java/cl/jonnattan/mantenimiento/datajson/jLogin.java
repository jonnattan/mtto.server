package cl.jonnattan.mantenimiento.datajson;

import lombok.Data;

/**
 * Clase de Credenciales
 * 
 * @author Jonnattan Griffiths
 * @since Programa de Mantenimiento
 * @version 1.0 del 22-12-2019
 * 
 */
public @Data class jLogin
{
  private String user;
  private String pass;

  public jLogin()
  {
    this(null, null);
  }

  public jLogin(String user, String pass)
  {
    super();
    this.user = user;
    this.pass = pass;
  }

}
