package cl.jonnattan.mantenimiento.datajson;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jResponse extends AbsKeylable
{
  private String  checkList;
  private String  titleCheckList;
  private Boolean yes;
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private Date    date;

  public jResponse()
  {
    super();
  }

}
