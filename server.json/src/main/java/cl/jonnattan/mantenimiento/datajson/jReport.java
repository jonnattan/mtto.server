package cl.jonnattan.mantenimiento.datajson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import cl.jonnattan.mantenimiento.datajson.abs.AbsKeylable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public @Data class jReport extends AbsKeylable
{
  private String  comentario;
  private Boolean hayFalla;
  private String  falla;
  private String  persona;
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private Date    fechahora;
  private Boolean externo;
  private Boolean pasa;
  private Long    idEq;

  private List<jCheckResp> respuestas;

  public jReport()
  {
    super();
    respuestas = new ArrayList<>();
  }

}
