INSERT INTO `mantenimientodb`.`personas` ( `id_persona`, `nombre`, `edad`, `cargo`, `usuario`, `pass`, `fecha_creacion`,`correo` ) values(1,'Administrador Oficial', 30, 'administrador', 'admin', 'admin', '2020-1-1', 'admin@gmail.com');

INSERT INTO `mantenimientodb`.`ubicaciones` (`id_ubicacion`, `descripcion_lugar`, `zona_ubicacion`) VALUES (1, 'Terminal', 'Instalaciones terrestres');
INSERT INTO `mantenimientodb`.`ubicaciones` (`id_ubicacion`, `descripcion_lugar`, `zona_ubicacion`) VALUES (2, 'Fondeadero Terminal', 'Instalaciones marítimas');
INSERT INTO `mantenimientodb`.`ubicaciones` (`id_ubicacion`, `descripcion_lugar`, `zona_ubicacion`) VALUES (3, 'Fondeadero y Terminal', 'Instalaciones marítimas y terrestres');

INSERT INTO `mantenimientodb`.`planos_referencia` (`id_plano_ref`, `extension_archivo`, `nombre_plano`, `ruta_plano`) VALUES (1, 'pdf', 'manuales del fabricante', './/manuales/');

INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (1, '2019-1-1', 'PLAN-30','generica', 30);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (2, '2019-1-1', 'PLAN-5','generica', 5);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (3, '2019-1-1', 'PLAN-36','generica', 36);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (4, '2019-1-1', 'PLAN-13','generica', 13);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (5, '2019-1-1', 'PLAN-1','generica', 1);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (6, '2019-1-1', 'PLAN-2','generica', 2);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (7, '2019-1-1', 'PLAN-26','generica', 26);
INSERT INTO `mantenimientodb`.`plan_mtto` (`id_plan_mtto`, `ultima_rev`, `nombre`,`version`,`periodo_dias` ) VALUES (8, '2019-1-1', 'PLAN-3','generica', 3);

INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (1, '2020-1-1 00:00:00.000000', 'Acumulador de Aire Comprimido', '0000000000','0000000000','aire', 1, 1, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (2, '2020-1-1 00:00:00.000000', 'Baliza de Señalizacion', '0000000000','0000000000', 'baliza', 1, 2, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (3, '2020-1-1 00:00:00.000000', 'Barrera de porteria', '0000000000','0000000000', 'aire', 1, 3, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (4, '2020-1-1 00:00:00.000000', 'Bomba de circuito de agua #1 (E-202)', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (5, '2020-1-1 00:00:00.000000', 'Bomba de circuito de agua #2 (E-202)', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (6, '2020-1-1 00:00:00.000000', 'Bomba de carga de Escaid 110', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (7, '2020-1-1 00:00:00.000000', 'Bomba de Carga de Soda Cáustica', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (8, '2020-1-1 00:00:00.000000', 'Bomba de Carga de Solvex', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (9, '2020-1-1 00:00:00.000000', 'Bomba Estanque Agua Potable E-301', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (10, '2020-1-1 00:00:00.000000', 'Bomba Estanque Agua Potable E-302', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (11, '2020-1-1 00:00:00.000000', 'Bomba Estanque E-201', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (12, '2020-1-1 00:00:00.000000', 'Bomba N°1 Lavador de Gases', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (13, '2020-1-1 00:00:00.000000', 'Bomba N°2 Lavador de Gases', '0000000000','0000000000', 'bomba', 1, 4, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (14, '2020-1-1 00:00:00.000000', 'Boya de Amarre de Popa 1', '0000000000','0000000000', 'boya', 2, 5, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (15, '2020-1-1 00:00:00.000000', 'Boya de Amarre de Popa 2', '0000000000','0000000000', 'boya', 2, 5, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (16, '2020-1-1 00:00:00.000000', 'Boya de Amarre de Proa', '0000000000','0000000000', 'boya', 2, 5, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (17, '2020-1-1 00:00:00.000000', 'Boyarín de Señalización de Fondeo', '0000000000','0000000000', 'boyarín', 2, 6, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (18, '2020-1-1 00:00:00.000000', 'Boyarín de Señalización de NaSH', '0000000000','0000000000', 'boyarín', 2, 6, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (19, '2020-1-1 00:00:00.000000', 'Boyarín de Señalización de Soda Cáustica', '0000000000','0000000000', 'boyarín', 2, 6, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (20, '2020-1-1 00:00:00.000000', 'Boyarín de Señalización Escaid 110', '0000000000','0000000000', 'boyarín', 2, 6, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (21, '2020-1-1 00:00:00.000000', 'Boyarín de Señalización PLEM', '0000000000','0000000000', 'boyarín', 2, 6, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (22, '2020-1-1 00:00:00.000000', 'Cámaras CCVT', '0000000000','0000000000', 'cctv', 2, 7, 1);
INSERT INTO `mantenimientodb`.`equipos` (`id_equipo`, `fecha_instalacion`, `nombre`, `numero_parte`, `numero_serie`, `tag`, `id_ubicacion`, `id_plan_mtto`, `id_plano_ref`) VALUES (23, '2020-1-1 00:00:00.000000', 'Cañerías de Agua', '0000000000','0000000000', 'agua', 3, 8, 1);


INSERT INTO `mantenimientodb`.`componentes` (`id_componente`, `descripcion`, `nombre`, `numero_parte`, `cantidad`, `numero_serie`, `id_equipo`) VALUES (1, 'Componente 1', 'Tuerca Maestra', '12345', '5', '22112233', 1);

INSERT INTO `mantenimientodb`.`reporte_mtto` (`id_reporte_mtto`, `comentario`, `fecha_mtto`, `requiere_externo`, `con_falla`, `razon_falla`, `pasada`, `id_persona`, `id_equipo`) VALUES (1, 'reporte comentario', '2019-11-25 17:00:00', True, True, 'Desconocida', True, 1, 1);
INSERT INTO `mantenimientodb`.`reporte_mtto` (`id_reporte_mtto`, `comentario`, `fecha_mtto`, `requiere_externo`, `con_falla`, `razon_falla`, `pasada`, `id_persona`, `id_equipo`) VALUES (2, 'todo bien', '2019-12-25 17:00:00', True, True, 'No hay falla', True, 1, 2);


INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (1, 'Limpieza Exterior', 'Limpieza', 1);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (2, 'Verificar Operatividad del Nanometro', 'Verificar Nanometro', 1);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (3, 'Purgar Contenedor de Aire', 'Purgar Contenedor', 1);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (4, 'Inspección visual de funcionamiento', 'Inspección Visual', 2);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (5, 'Limpieza Exterior e Inspeccionar correcto funcionamiento de botonera de partida, y que no haya fugas', 'Limpieza e Inspección', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (6, 'Inspeccionar equipo y verificar filtro', 'Verificar filtro', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (7, 'Verificar estado de pintura exterior y omega', 'Verificar estado de pintura', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (8, 'Desmontar y efectuar limpieza a filtro del circuito del admisión de la bomba', 'Desmontar filtro interior', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (9, 'Efectuar mantenimiento exterior, pintado exterior', 'Mantenimiento exterior', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (10, 'Efectuar medición de vibraciones', 'Medicion de vibraciones', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (11, 'Reacondicionar total o parcial', 'Re acondicionamiento', 4);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (12, 'Efectuar lavado de superficie de la boya', 'Lavado de superfie', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (13, 'Engrase de Ganchos de Escape', 'Engrase de ganchos', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (14, 'Limpieza e inspección visual de baliza solar', 'Limpieza e inspección', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (15, 'Revisión de cables y verduguetes', 'Revisión de Cables', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (16, 'Mantención de bienal (Ref. Circular Marítima 0-71/034 de la D.G.T.M y M.M.)', 'Mantención bienal', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (17, 'Re acondicionar total o parcialmente', 'Re acondicionar', 5);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (18, 'Inspección visual de cable de orinque, baliza solar y boyarín', 'Inspección visual', 6);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (19, 'Cambio de boyarín', 'Cambio boyarín', 6);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (20, 'Efectuar prueba de operación y funcionamiento (semanalmente)', 'Efectuar pruebas', 7);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (21, 'Inspección visual para buscar evidencia de fugas (D. S. 43 Art. 28)', 'Inspección visual fugas', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (22, 'Inspección visual para verificar desalineamientos', 'Inspección visual desalineamientos', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (23, 'Inspección visual para verificar desalineamientos', 'Inspección visual desalineamientos', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (24, 'Inspección visual para verificar presencia de corroción', 'Inspección visual corrosión', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (25, 'Inspección visual para verificar estados de los soportes', 'Inspección visual estado soportes', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (26, 'Efectuar Prueba de Hermeticidad', 'Prueba de hermeticidad', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (27, 'Prueba de Presión de Trabajo por 24 hrs.', 'Prueba de presión', 8);
INSERT INTO `mantenimientodb`.`checklist` (`id_checklist`, `descripcion`, `titulo`, `id_plan_mtto`) VALUES (28, 'Pintado de tramo terrestre de cañería', 'Pintado cañería', 8);


INSERT INTO `mantenimientodb`.`respuesta_mtto` (`id_respuesta`, `respuesta_check`, `date`, `id_checklist`, `id_reporte_mtto`) VALUES (1, True, '2019-11-27 18:00:00.000000', 1, 1);
INSERT INTO `mantenimientodb`.`respuesta_mtto` (`id_respuesta`, `respuesta_check`, `date`, `id_checklist`, `id_reporte_mtto`) VALUES (2, False, '2019-11-27 18:01:00.000000', 2, 1);
INSERT INTO `mantenimientodb`.`respuesta_mtto` (`id_respuesta`, `respuesta_check`, `date`, `id_checklist`, `id_reporte_mtto`) VALUES (3, True, '2019-11-27 18:00:00.000000', 3, 1);
INSERT INTO `mantenimientodb`.`respuesta_mtto` (`id_respuesta`, `respuesta_check`, `date`, `id_checklist`, `id_reporte_mtto`) VALUES (4, True, '2019-11-27 18:00:00.000000', 4, 2);