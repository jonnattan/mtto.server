package cl.jonnattan.mantenimiento;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReport;

// @RequestScope // con esto no sera sounu 1 instancia del componente, sino que se crea y destruye 
// @SessionScope // para sesiones (el alcance es mientras dura la sesion) termian cuando se cierra el navegador o por timeout. no aplica destroy
// @ApplicationScope // Es muy parecido al singleton. 

@RestController
@RequestMapping("/")
public class CrudControllers
{
  private final static Logger logger = Logger.getLogger(CrudControllers.class.getName());

  @Autowired
  private PlanService planService;
  @Autowired
  private PersonaService perService;
  @Autowired
  private MttoService mttoService;
  @Autowired
  CompService            compService;
  @Autowired
  ChekListService        checkService;

  @RequestMapping(value = "/plandelete/{id}", method = RequestMethod.GET)
  public ResponseEntity<?> deletePlan(@PathVariable Long id)
  {
    logger.info("Se elimina plan");
    planService.delete(id);
    return new ResponseEntity<>( HttpStatus.ACCEPTED);
  }

  @RequestMapping(value = "/persondelete/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> persondelete(@PathVariable Long id)
  {
    logger.info("Se elimina objeto id" + id);
    perService.delete(id);
    return new ResponseEntity<>( HttpStatus.ACCEPTED);
  }
  
  @RequestMapping(value = "/reportdelete/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> reportdelete(@PathVariable Long id)
  {
    logger.info("Se elimina objeto id" + id);
    mttoService.delete(id);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
  
  @RequestMapping(value = "/componentdelete/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> componentdelete(@PathVariable Long id)
  {
    logger.info("Se elimina objeto id" + id);
    compService.delete(id);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
  
  @RequestMapping(value = "/checklistdelete/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> checklistdelete(@PathVariable Long id)
  {
    logger.info("Se elimina objeto id" + id);
    checkService.delete(id);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
//---------------------------------------------------------------------------
  @RequestMapping(value = "/plansave", method = RequestMethod.POST)
  public ResponseEntity<Long> create(@Valid @RequestBody jPlan plan)
  {
    Long    success = planService.save(plan);
    HttpStatus resp    = (success != -1L ) ? HttpStatus.CREATED : HttpStatus.CONFLICT;
    return new ResponseEntity<Long>(success, resp);
  }

  @RequestMapping(value = "/personsave", method = RequestMethod.POST)
  public ResponseEntity<Long> save(@Valid @RequestBody jPerson dato)
  {
    Long    success = perService.save(dato);
    HttpStatus resp    = (success != -1L) ? HttpStatus.CREATED : HttpStatus.CONFLICT;
    return new ResponseEntity<Long>(success, resp);
  }

  
  @RequestMapping(value = "/reportssave", method = RequestMethod.POST)
  public ResponseEntity<Long> create(@Valid @RequestBody jReport mtto)
  {
    Long    id = mttoService.create(mtto);
    HttpStatus resp    = (id != -1 ) ? HttpStatus.CREATED : HttpStatus.CONFLICT;
    return new ResponseEntity<Long>(id, resp);
  }
  
  @RequestMapping(value = "/componentsave", method = RequestMethod.POST)
  public ResponseEntity<Long> componentsave(@Valid @RequestBody jComponent dato)
  {
    Long    id = compService.save(dato);
    HttpStatus resp    = (id != -1 ) ? HttpStatus.CREATED : HttpStatus.CONFLICT;
    return new ResponseEntity<Long>(id, resp);
  }
  
  @RequestMapping(value = "/checklistsave", method = RequestMethod.POST)
  public ResponseEntity<Long> checklistsave(@Valid @RequestBody jChecklist dato)
  {
    Long    id = checkService.save(dato);
    HttpStatus resp    = (id != -1 ) ? HttpStatus.CREATED : HttpStatus.CONFLICT;
    return new ResponseEntity<Long>(id, resp);
  }
}
