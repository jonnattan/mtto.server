package cl.jonnattan.mantenimiento;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jLogin;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jResponse;

// @RequestScope // con esto no sera sounu 1 instancia del componente, sino que se crea y destruye 
// @SessionScope // para sesiones (el alcance es mientras dura la sesion) termian cuando se cierra el navegador o por timeout. no aplica destroy
// @ApplicationScope // Es muy parecido al singleton. 

@RestController
@RequestMapping("/")
public class MainController
{
  private final static Logger logger = Logger.getLogger(MainController.class.getName());

  @Autowired
  private EqService      eqService;
  @Autowired
  private PersonaService perService;
  @Autowired
  ResponseService        respService;
  @Autowired
  MttoService            mttoService;
  @Autowired
  CompService            compService;
  @Autowired
  ChekListService        checkService;
  @Autowired
  private PlanService    planService;

  @PostMapping(value = "/login") // @RequestMapping(value = "/login", method = RequestMethod.POST)
  public ResponseEntity<jPerson> login(@Valid @RequestBody jLogin credencial)
  {
    jPerson    person = perService.login(credencial);
    HttpStatus resp   = person != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
    return new ResponseEntity<jPerson>(person, resp);
  }

  @RequestMapping(value = "/equipments", method = RequestMethod.GET)
  public ResponseEntity<List<jEquipment>> equipments()
  {
    logger.info("Se Piden Equipos");
    List<jEquipment> list = eqService.getEquipment();
    HttpStatus       resp = (list != null && !list.isEmpty()) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jEquipment>>(list, resp);
  }

  @RequestMapping(value = "/findresponses/{id}", method = RequestMethod.GET)
  public ResponseEntity<List<jResponse>> findresponses(@PathVariable Long id)
  {
    logger.info("Se buscamos las respuestas de un reporte");
    List<jResponse> list = respService.getResponseOfReport(id);
    HttpStatus      resp = (list != null && !list.isEmpty()) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jResponse>>(list, resp);
  }

  @RequestMapping(value = "/findequipment/{id}", method = RequestMethod.GET)
  public ResponseEntity<jFullEquipment> findEquipment(@PathVariable Long id)
  {
    logger.info("Se pregunta por Equipo: " + id);
    jFullEquipment eq   = eqService.findEquipment(id);
    HttpStatus     resp = (eq != null) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<jFullEquipment>(eq, resp);
  }

  @RequestMapping(value = "/pending", method = RequestMethod.GET)
  public ResponseEntity<List<String>> pending()
  {
    logger.info("Se pregunta por revisiones pendientes");
    List<String> list = mttoService.getPendingEq();
    HttpStatus   resp = (list != null) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<String>>(list, resp);
  }

  @RequestMapping(value = "/checks", method = RequestMethod.GET)
  public ResponseEntity<List<jChecklist>> checks()
  {
    logger.info("Se pregunta por revisiones pendientes");
    List<jChecklist> list = checkService.getAll();
    HttpStatus       resp = (list != null) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jChecklist>>(list, resp);
  }

  @RequestMapping(value = "/components", method = RequestMethod.GET)
  public ResponseEntity<List<jComponent>> components()
  {
    logger.info("Se pregunta por revisiones pendientes");
    List<jComponent> list = compService.getAll();
    HttpStatus       resp = (list != null) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jComponent>>(list, resp);
  }

  @RequestMapping(value = "/plans", method = RequestMethod.GET)
  public ResponseEntity<List<jPlan>> getplans()
  {
    logger.info("Se piden todos los planes");
    List<jPlan> list = planService.getPlans();
    HttpStatus  resp = (list != null && !list.isEmpty()) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jPlan>>(list, resp);
  }

  // ---------------------------------------------------------------------------
  @RequestMapping(value = "/persons", method = RequestMethod.GET)
  public ResponseEntity<List<jPerson>> getpersons()
  {
    logger.info("Se piden todos los planes");
    List<jPerson> list = perService.getPersons();
    HttpStatus    resp = (list != null && !list.isEmpty()) ? HttpStatus.OK : HttpStatus.CONFLICT;
    return new ResponseEntity<List<jPerson>>(list, resp);
  }

}
