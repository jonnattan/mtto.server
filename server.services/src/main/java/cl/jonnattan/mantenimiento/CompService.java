package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoComponents;
import cl.jonnattan.mantenimiento.daos.IDaoEquipment;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class CompService
{
  private final static Logger logger = Logger.getLogger(CompService.class.getName());

  @Autowired
  EntityManager  em;
  @Autowired
  IDaoComponents dao;
  @Autowired
  IDaoEquipment               daoEq;
  @Autowired
  IConvert       conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Long save(final jComponent aValue)
  {
    Long      idSaved = -1L;
    Component dato    = conversor.convert(aValue);
    Optional<Equipment> result = daoEq.findById( aValue.getIdEq() );
    if( result != null && result.isPresent() )
      dato.setEquipo( result.get() );
    if (dato != null) {
      Component obj = dao.save(dato);
      if (obj != null)
        idSaved = obj.getId();
    }
    return idSaved;
  }

  @Transactional
  public void delete(final Long aValue)
  {
    dao.deleteById(aValue);
  }

  @Transactional(readOnly = true)
  public List<jComponent> getAll()
  {
    List<jComponent>    list   = null;
    Iterable<Component> result = dao.findAll();
    if (result != null) {
      list = new ArrayList<>();
      for (Component dato : result) {
        jComponent jDato = conversor.convert(dato);
        list.add(jDato);
      }
    } else
      logger.info("No encontro nada...");
    return list;
  }

}
