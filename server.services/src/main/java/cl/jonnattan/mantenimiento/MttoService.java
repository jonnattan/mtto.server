package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoCheckList;
import cl.jonnattan.mantenimiento.daos.IDaoEquipment;
import cl.jonnattan.mantenimiento.daos.IDaoPerson;
import cl.jonnattan.mantenimiento.daos.IDaoReportMtto;
import cl.jonnattan.mantenimiento.datajson.jCheckResp;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class MttoService
{
  private final static Logger logger = Logger.getLogger(MttoService.class.getName());
  private final static long   DAY_MS = 86400000L;
  @Autowired
  EntityManager               em;
  @Autowired
  IDaoCheckList               daoCheclisk;
  @Autowired
  IDaoReportMtto              daoRte;
  @Autowired
  IDaoPerson                  daoPerson;
  @Autowired
  IDaoEquipment               daoEq;
  @Autowired
  IConvert                    conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }


  @Transactional
  public void delete(final Long aValue)
  {
    daoRte.deleteById(aValue);
  }
  
  @Transactional
  public Long create(final jReport aDato)
  {
    ReportMtto        mtto = conversor.convert(aDato);
    Optional<Persona> psn  = daoPerson.findAllByUser(aDato.getPersona());
    if (psn.isPresent())
      mtto.setPerson(psn.get());
    Long                idEq = aDato.getIdEq();
    Optional<Equipment> eq   = daoEq.findById(idEq);
    if (eq.isPresent())
      mtto.setEquipo(eq.get());
    List<jCheckResp> list     = aDato.getRespuestas();
    List<Response>   listRtas = mtto.getReports();
    for (jCheckResp rsp : list) {
      Optional<CheckList> chl = daoCheclisk.findById(rsp.getId());
      if (chl.isPresent()) {
        CheckList cl        = chl.get();
        Response  respuesta = new Response();
        respuesta.setCheck(cl);
        respuesta.setChecked(rsp.getYes());
        listRtas.add(respuesta);
      }
    }

    ReportMtto rta = null;
    if (mtto != null)
      rta = daoRte.save(mtto);
    if(rta != null)
    return rta.getId();
    else
      return -1L;
  }

  @Transactional(readOnly = true)
  public jReport getRtteMtto(Long id)
  {
    jReport              ret    = null;
    Optional<ReportMtto> result = daoRte.findById(id);
    if (result.isPresent()) {
      // ret = Conver.convert(result.get());
    } else
      logger.info("No encontro nada... retorna NULL");
    return ret;
  }

  @Transactional(readOnly = true)
  public List<String> getPendingEq()
  {
    List<String>        lista   = null;
    Iterable<Equipment> eqs     = daoEq.findAll();
    Date                current = new Date();
    for (Equipment eq : eqs) {
      lista = new ArrayList<>();
      List<ReportMtto> rttes = eq.getReports();
      int              dias  = eq.getPlan().getPeriodo();
      // Los reportes estan ordenados por FECHA
      if (!rttes.isEmpty()) {
        Date last    = rttes.get(0).getDate();
        long toca_ms = last.getTime() + (dias * DAY_MS);
        Date toca    = new Date(toca_ms);
        if (toca.before(current))
          lista.add(eq.getName());
      }
    }
    return lista;
  }
}
