package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoEquipment;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class EqService
{
  private final static Logger logger = Logger.getLogger(EqService.class.getName());

  @Autowired
  EntityManager     em;
  @Autowired
  IDaoEquipment     dao;
  @Autowired
  IConvert          conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Boolean create(final jEquipment eq)
  {
    Equipment cop = conversor.convert(eq);
    if (cop != null)
      dao.save(cop);
    return (cop != null);
  }

  @Transactional(readOnly = true)
  public List<jEquipment> getEquipment()
  {
    List<jEquipment>    lista  = new ArrayList<>();
    Iterable<Equipment> result = dao.findAll();
    if (result != null)
      result.forEach((final Equipment aValue) -> {
        jEquipment eq = conversor.convert(aValue);
        eq.setNamePlan(aValue.getPlan().getName());
        List<ReportMtto> mttos = aValue.getReports();
        long             last     = 0;
        String           detail   = "Sin comentarios";
        for( ReportMtto rtte : mttos )
        {
          Date fecha = rtte.getDate();          
          if (fecha != null && fecha.getTime() > last)
            detail = rtte.getComment();
        }
        eq.setLastDetail(detail);
        lista.add(eq);
      });
    else
      logger.info("No encontro nada...");
    return lista;
  }

  public jFullEquipment findEquipment(final Long id)
  {
    jFullEquipment eqFull = null;
    Optional<Equipment> result = dao.findById(id);
    if (result.isPresent()) {
      Equipment eq = result.get();
      eqFull = conversor.convertFull( eq );
    }
    return eqFull;
  }
}
