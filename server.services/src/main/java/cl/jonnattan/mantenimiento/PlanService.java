package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoPlan;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class PlanService
{
  private final static Logger logger = Logger.getLogger(PlanService.class.getName());

  @Autowired
  EntityManager em;
  @Autowired
  IDaoPlan      dao;
  @Autowired
  IConvert      conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Long save(final jPlan aValue)
  {
    Long  id  = -1L;
    PlanMtto dato = conversor.convert(aValue);
    if (dato != null)
    {
      PlanMtto obj = dao.save(dato);
      if(obj != null)
        id = obj.getId();
    }
    return id;
  }

  @Transactional
  public void delete(final Long aValue)
  {
     dao.deleteById(aValue);
  }
  
  @Transactional(readOnly = true)
  public List<jPlan> getPlans()
  {
    List<jPlan>           planes = null;
    Iterable<PlanMtto> result = dao.findAll();
    if (result != null) {
      planes = new ArrayList<>();
      for( PlanMtto mtto : result )
      {
        jPlan jplan = conversor.convert(mtto);
        planes.add(jplan);
      }
    } else
      logger.info("No encontro nada...");
    return planes;
  }

}
