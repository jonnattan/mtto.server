package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoCheckList;
import cl.jonnattan.mantenimiento.daos.IDaoPlan;
import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class ChekListService
{
  private final static Logger logger = Logger.getLogger(ChekListService.class.getName());

  @Autowired
  EntityManager em;
  @Autowired
  IDaoCheckList dao;
  @Autowired
  IDaoPlan      daoPlan;
  @Autowired
  IConvert      conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Long save(final jChecklist aValue)
  {
    Long      idSaved = -1L;
    CheckList dato    = conversor.convert(aValue);
    Optional<PlanMtto> result = daoPlan.findById( aValue.getIdPlan() );
    if(result!= null && result.isPresent() )
      dato.setPlan( result.get() );
    if (dato != null) {
      CheckList obj = dao.save(dato);
      if (obj != null)
        idSaved = obj.getId();
    }
    return idSaved;
  }

  @Transactional
  public void delete(final Long aValue)
  {
    dao.deleteById(aValue);
  }

  @Transactional(readOnly = true)
  public List<jChecklist> getAll()
  {
    List<jChecklist>    list   = null;
    Iterable<CheckList> result = dao.findAll();
    if (result != null) {
      list = new ArrayList<>();
      for (CheckList dato : result) {
        jChecklist jDato = conversor.convert(dato);
        list.add(jDato);
      }
    } else
      logger.info("No encontro nada...");
    return list;
  }

}
