package cl.jonnattan.mantenimiento.interfaces;

public interface IUDPComm
{
  public boolean write( final byte[] aData );
}
