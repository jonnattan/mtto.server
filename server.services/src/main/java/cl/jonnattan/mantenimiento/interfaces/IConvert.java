package cl.jonnattan.mantenimiento.interfaces;

import cl.jonnattan.mantenimiento.CheckList;
import cl.jonnattan.mantenimiento.Component;
import cl.jonnattan.mantenimiento.Equipment;
import cl.jonnattan.mantenimiento.Location;
import cl.jonnattan.mantenimiento.Persona;
import cl.jonnattan.mantenimiento.PlanMtto;
import cl.jonnattan.mantenimiento.Reference;
import cl.jonnattan.mantenimiento.ReportMtto;
import cl.jonnattan.mantenimiento.Response;
import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jLocation;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReference;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.jResponse;

public interface IConvert
{
  public Equipment convert(final jEquipment aEq);

  public jEquipment convert(final Equipment aEq);

  public Persona convert(final jPerson aValue);

  public jPerson convert(final Persona aValue);

  public Location convert(final jLocation aValue);

  public jLocation convert(final Location aValue);

  public PlanMtto convert(final jPlan aValue);

  public jPlan convert(final PlanMtto aValue);
  
  public ReportMtto convert(final jReport aValue);

  public jReport convert(final ReportMtto aValue);

  public Component convert(final jComponent aValue);

  public jComponent convert(final Component aValue);

  public jReference convert(final Reference aValue);

  public Reference convert(final jReference aValue);
  
  public jChecklist convert(final CheckList aValue);

  public CheckList convert(final jChecklist aValue);
  
  public jResponse convert(final Response aValue);

  public Response convert(final jResponse aValue);

  public jFullEquipment convertFull(final Equipment aEq);
}
