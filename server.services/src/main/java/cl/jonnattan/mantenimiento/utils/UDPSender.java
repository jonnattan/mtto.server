package cl.jonnattan.mantenimiento.utils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import cl.jonnattan.mantenimiento.interfaces.IUDPComm;

public class UDPSender implements IUDPComm
{
  private final static Logger logger = Logger.getLogger(UDPSender.class.getName());
  private InetAddress         server = null;
  private DatagramSocket      socket = null;
  @Value("${udpserver.port}")
  private String              port;
  private int                 puerto;
  @Value("${udpserver.address}")
  private String              address;
  private DatagramPacket      output = null;

  public UDPSender()
  {
    super();
  }

  @PostConstruct
  private void initialize()
  {
    try {
      this.socket = new DatagramSocket();
      this.server = InetAddress.getByName(address);
      int puerto = Integer.parseInt(port);
      logger.info("HOST " + address + ":" + puerto);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Escribe la data en el puerto
   * 
   * @param bArray
   */
  @Override
  public boolean write(final byte[] aData)
  {
    boolean success = false;
    output = new DatagramPacket(aData, aData.length, server, puerto);
    try {
      socket.send(output);
      logger.info("Send " + aData.length + " bytes to Port UDP[" + port + "] ");
      success = true;
    } catch (IOException ex) {
      ex.printStackTrace();
      success = false;
    }
    return success;
  }
}
