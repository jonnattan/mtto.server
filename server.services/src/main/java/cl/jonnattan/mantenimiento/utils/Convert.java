package cl.jonnattan.mantenimiento.utils;

import java.util.List;

import cl.jonnattan.mantenimiento.CheckList;
import cl.jonnattan.mantenimiento.Component;
import cl.jonnattan.mantenimiento.Equipment;
import cl.jonnattan.mantenimiento.Location;
import cl.jonnattan.mantenimiento.Persona;
import cl.jonnattan.mantenimiento.PlanMtto;
import cl.jonnattan.mantenimiento.Reference;
import cl.jonnattan.mantenimiento.ReportMtto;
import cl.jonnattan.mantenimiento.Response;
import cl.jonnattan.mantenimiento.datajson.jChecklist;
import cl.jonnattan.mantenimiento.datajson.jComponent;
import cl.jonnattan.mantenimiento.datajson.jEquipment;
import cl.jonnattan.mantenimiento.datajson.jFullEquipment;
import cl.jonnattan.mantenimiento.datajson.jLocation;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.datajson.jPlan;
import cl.jonnattan.mantenimiento.datajson.jReference;
import cl.jonnattan.mantenimiento.datajson.jReport;
import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

public class Convert implements IConvert
{
  public Convert()
  {
    // TODO Auto-generated constructor stub
  }

  @Override
  public jEquipment convert(final Equipment aEq)
  {
    if (aEq == null)
      return null;
    jEquipment dato = new jEquipment();
    dato.setId(aEq.getId());
    dato.setName(aEq.getName());
    dato.setPartNumber(aEq.getPartnumber());
    dato.setSerialNumber(aEq.getSerialNumber());
    return dato;
  }

  @Override
  public Equipment convert(final jEquipment aEq)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Persona convert(final jPerson aValue)
  {
    if (aValue == null)
      return null;
    Persona dato = new Persona();
    dato.setId(aValue.getId());
    dato.setName(aValue.getNombre());
    dato.setUser(aValue.getUser());
    dato.setPassword(aValue.getPass());
    dato.setAge(aValue.getEdad());
    dato.setMail(aValue.getCorreo());
    dato.setRol(aValue.getCargo());
    return dato;
  }

  @Override
  public jPerson convert(final Persona aValue)
  {
    if (aValue == null)
      return null;
    jPerson jperson = new jPerson();
    jperson.setId(aValue.getId());
    jperson.setUser(aValue.getUser());
    jperson.setPass(aValue.getPassword());
    jperson.setNombre(aValue.getName());
    jperson.setCorreo(aValue.getMail());
    jperson.setCargo(aValue.getRol());
    jperson.setEdad(aValue.getAge());
    return jperson;
  }

  @Override
  public jFullEquipment convertFull(final Equipment aValue)
  {
    if (aValue == null)
      return null;
    jFullEquipment eq = new jFullEquipment();
    eq.setId(aValue.getId());
    eq.setName(aValue.getName());
    eq.setSerialNumber(aValue.getSerialNumber());
    eq.setPartNumber(aValue.getPartnumber());
    eq.setInstallDate(aValue.getInstall());
    eq.setTag(aValue.getTag());

    eq.setLocation(convert(aValue.getLocation()));
    eq.setReference(convert(aValue.getReference()));
    eq.setPlan(convert(aValue.getPlan()));

    List<Component> listc = aValue.getComponents();
    for (Component cmp : listc)
      eq.getComponents().add(convert(cmp));

    List<ReportMtto> listm = aValue.getReports();
    for (ReportMtto mnt : listm)
      eq.getMaintenances().add(convert(mnt));

    return eq;
  }

  @Override
  public Location convert(final jLocation aValue)
  {
    if (aValue == null)
      return null;
    return null;
  }

  @Override
  public jLocation convert(final Location aValue)
  {
    if (aValue == null)
      return null;
    jLocation jloc = new jLocation();
    jloc.setId(aValue.getId());
    jloc.setZona(aValue.getZone());
    jloc.setLugar(aValue.getDescription());
    return jloc;
  }

  @Override
  public ReportMtto convert(final jReport aValue)
  {
    if (aValue == null)
      return null;
    ReportMtto dato = new ReportMtto();
    dato.setId(aValue.getId());
    dato.setComment(aValue.getComentario());
    dato.setExternAgent(aValue.getExterno());
    dato.setFailure(aValue.getHayFalla());
    dato.setReasonFailure(aValue.getFalla());
    // Falta Persona...
    return dato;
  }

  @Override
  public jReport convert(final ReportMtto aValue)
  {
    if (aValue == null)
      return null;
    jReport rtte = new jReport();
    rtte.setId(aValue.getId());
    rtte.setPasa( aValue.getOk() );
    rtte.setComentario(aValue.getComment());
    rtte.setHayFalla(aValue.getFailure());
    rtte.setFalla(aValue.getReasonFailure());
    rtte.setPersona(aValue.getPerson().getName());
    rtte.setExterno(aValue.getExternAgent());
    rtte.setFechahora(aValue.getDate());
    return rtte;
  }

  @Override
  public Component convert(jComponent aValue)
  {
    if (aValue == null)
      return null;
    Component dato = new Component();
    dato.setId( aValue.getId());
    dato.setDescription(aValue.getDescripcion());
    dato.setName( aValue.getName() );
    dato.setPartNumber(aValue.getPartnumber());
    dato.setSerialNumber(aValue.getSerialNumber());
    dato.setQuantity( aValue.getCantidad() );
    return dato;
  }

  @Override
  public jComponent convert(Component aValue)
  {
    if (aValue == null)
      return null;
    jComponent cmp = new jComponent();
    cmp.setId(aValue.getId());
    cmp.setName(aValue.getName());
    cmp.setDescripcion(aValue.getDescription());
    cmp.setPartnumber(aValue.getPartNumber());
    cmp.setCantidad(aValue.getQuantity());
    cmp.setIdEq( aValue.getEquipo().getId());
    return cmp;
  }

  @Override
  public jReference convert(final Reference aValue)
  {
    if (aValue == null)
      return null;
    jReference ref = new jReference();
    ref.setId(aValue.getId());
    ref.setExtend(aValue.getExtend());
    ref.setName(aValue.getName());
    ref.setPath(aValue.getPath());
    return ref;
  }

  @Override
  public Reference convert(jReference aValue)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PlanMtto convert(final jPlan aValue)
  {
    if (aValue == null)
      return null;
    PlanMtto dato = new PlanMtto();
    dato.setId(aValue.getId());
    dato.setName(aValue.getName());
    dato.setVersion(aValue.getVersion());
    return dato;
  }

  @Override
  public jPlan convert(final PlanMtto aValue)
  {
    if (aValue == null)
      return null;
    jPlan plan = new jPlan();
    plan.setId(aValue.getId());
    plan.setName(aValue.getName());
    plan.setRevision(aValue.getLastRev());
    plan.setVersion(aValue.getVersion());
    List<CheckList> list = aValue.getChecklists();
    for (CheckList cl : list) {
      plan.getItems().add(convert(cl));
    }
    return plan;
  }

  @Override
  public jChecklist convert(final CheckList aValue)
  {
    if (aValue == null)
      return null;
    jChecklist jcl = new jChecklist();
    jcl.setId(aValue.getId());
    jcl.setDescripcion(aValue.getDescription());
    jcl.setTitulo(aValue.getTitle());
    jcl.setIdPlan( aValue.getPlan().getId() );
    return jcl;
  }

  @Override
  public CheckList convert(jChecklist aValue)
  {
    if (aValue == null)
      return null;
    CheckList dato = new CheckList();
    dato.setId(aValue.getId());
    dato.setTitle(aValue.getTitulo());
    dato.setDescription(aValue.getDescripcion());
    return dato;
  }

  @Override
  public jResponse convert(final Response aValue)
  {
    if (aValue == null)
      return null;
    jResponse jr = new jResponse();
    jr.setId(aValue.getId());
    jr.setCheckList(aValue.getCheck().getDescription());
    jr.setTitleCheckList(aValue.getCheck().getTitle());
    jr.setDate(aValue.getDate());
    jr.setYes(aValue.getChecked());
    
    return jr;
  }

  @Override
  public Response convert(jResponse aValue)
  {
    // TODO Auto-generated method stub
    return null;
  }

}
