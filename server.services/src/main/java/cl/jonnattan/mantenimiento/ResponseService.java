package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoReportMtto;
import cl.jonnattan.mantenimiento.datajson.jResponse;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

/**
 * Clase sin descripcion aun
 * 
 * @author Jonnattan Griffiths
 * @since Programa de Mantenimiento
 * @version 1.0 del 28-12-2019
 * 
 */
@Service
public class ResponseService
{
  private final static Logger logger = Logger.getLogger(EqService.class.getName());

  @Autowired
  EntityManager  em;
  @Autowired
  IConvert       conversor;
  @Autowired
  IDaoReportMtto dao;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Boolean create(final jResponse dato)
  {
    /*
     * Response cop = conversor.convert(dato); if (cop != null) dao.save(cop);
     * return (cop != null);
     */
    return false;
  }

  public List<jResponse> getResponseOfReport(final Long id)
  {
    logger.info("Se solicitan las respuesta a reporte " + id);
    final List<jResponse> list   = new ArrayList<>();
    Optional<ReportMtto>  result = dao.findById(id);
    if (result.isPresent()) {
      ReportMtto     rtte = result.get();
      List<Response> rtas = rtte.getReports();
      if (rtas != null)
        rtas.forEach((final Response aValue) -> {
          jResponse jr = conversor.convert(aValue);
          // Pilleria... se pone el ID del CHECKLIST Asociado
          jr.setId(aValue.getCheck().getId());
          // ------------------------------------------------
          if (jr != null)
            list.add(jr);
        });
    }
    return list;
  }

}
