package cl.jonnattan.mantenimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.jonnattan.mantenimiento.daos.IDaoPerson;
import cl.jonnattan.mantenimiento.datajson.jLogin;
import cl.jonnattan.mantenimiento.datajson.jPerson;
import cl.jonnattan.mantenimiento.interfaces.IConvert;

@Service
public class PersonaService
{
  private final static Logger logger = Logger.getLogger(PersonaService.class.getName());

  @Autowired
  EntityManager em;
  @Autowired
  IDaoPerson    dao;
  @Autowired
  IConvert      conversor;

  @PostConstruct
  public void initialize()
  {
    // Se ejecuta despues de traer todos los atributos y justo al moemnto de
    // construccion
  }

  @PreDestroy
  public void destroy()
  {
    // cuando se destruye se ejecuta...
  }

  @Transactional
  public Long save(final jPerson aValue)
  {
    Long    idElelm = -1L;
    Persona dato    = conversor.convert(aValue);
    if (dato != null) {
      Persona obj = dao.save(dato);
      if (obj != null)
        idElelm = obj.getId();
    }
    return (idElelm);
  }

  @Transactional
  public void delete(final Long aValue)
  {
    dao.deleteById(aValue);
  }

  @Transactional(readOnly = true)
  public List<jPerson> getPersons()
  {
    List<jPerson>     list   = null;
    Iterable<Persona> result = dao.findAll();
    if (result != null) {
      list = new ArrayList<>();
      for (Persona mtto : result) {
        jPerson jplan = conversor.convert(mtto);
        list.add(jplan);
      }
    } else
      logger.info("No encontro nada...");
    return list;
  }

  @Transactional(readOnly = true)
  public jPerson login(final jLogin credencial)
  {
    jPerson           persona = null;
    Optional<Persona> result  = dao.findAllByUserAndPassword(credencial.getUser(), credencial.getPass());
    if (result.isPresent()) {
      persona = conversor.convert(result.get());
    } else
      logger.info("No encontro nada...");
    return persona;
  }

}
